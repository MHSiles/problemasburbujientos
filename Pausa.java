import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

public class Pausa implements GameState{
	
    private Image background;
    private GameContext gc;
    
    public Pausa() {
    	background = new Image("aon");
    }

	@Override
	public void paint(Graphics g) {
		background.paintImage(g, 0, 0, 800, 600);
		g.setFont(new Font("Trebuchet", Font.BOLD, 50));
		g.setColor(Color.white);
		g.drawString("Pausa", 320, 100);
		Scores.getInstance().paintNivel(g);
		g.setFont(new Font("Trebuchet", Font.BOLD, 40));
		g.drawString("Presiona [ P ] para continuar jugando", 40, 300);
		g.drawString("Presiona [ F5 ] para continuar jugando", 30, 350);
	}

	@Override
	public void update(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setContext(GameContext gc) {
		this.gc = gc;
		
	}

	@Override
	public void start() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void instrucciones() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pausa() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void gameover() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void nivel1() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void nivel2() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void burbujaSucia() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void scores() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void tienda() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int keyCode = e.getKeyCode();
		if(keyCode == KeyEvent.VK_P) {
			gc.pausa();
		}
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		
		
	}

	@Override
	public void scoreboard() {
		// TODO Auto-generated method stub
		
	}

}
