import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.Random;

public class Boss {
	private int size, posX, posY, hits, count;
	private double xSpeed, ySpeed, gravity;
	private double maxJumpSpeed;
	private boolean firstBounce, crash;
	private Image burbuja;
	
	public Boss(int y, int size, double gravity, String imagen){
		
		Random r = new Random();
		int x = Math.abs(r.nextInt()%600);
		
		burbuja = new Image(imagen);
		posX = x;
		posY = y;
		this.gravity = gravity;
		firstBounce = true;
		this.size = size;
		ySpeed = 0;
		xSpeed = 1;
		hits = 0;
		crash = false;
		count = 0;
	}
	
	public void paint(Graphics g){
        g.setFont(new Font("Trebuchet", Font.BOLD, 15));
        g.setColor(Color.RED);
        g.fillRect(20,20, 300, 25);
        g.setColor(Color.GREEN);
        g.fillRect(20,20,300-(hits*30),25);
		g.setColor(Color.black);
        g.drawString("BURBUJA SUCIA", 28, 38);
		burbuja.paintImage(g, posX, posY, size, size);
	}
	
	private void updateYSpeed(int screenY) {
		if(posY + size >= screenY-50) {
			if(firstBounce) {
				//Guardar la velocidad inicial para cada rebote
				maxJumpSpeed = -ySpeed;
				firstBounce = false;
			}
			
			//Resetear la velocidad para subir
			ySpeed = maxJumpSpeed;
		}
		ySpeed += gravity;
	}
	
	private void updateXSpeed(int screenX) {
		//Si llega a alguno de los extremos laterales
		if((posX + size >= screenX) || (posX <= 0)) {
			xSpeed = xSpeed * -1;
			if(getXSpeed() == 1){
				changeRoute("burbujaSucia");
			}else{
				changeRoute("burbujaSuciaL");
			}
		}	
	}
	
	public void update(int screenX, int screenY){
		movimiento(screenX, screenY);
		if(crash){
			if(count < 30){
				count++;
			}else{
				crash = false;
				count = 0;
				if(getXSpeed() == 1){
					changeRoute("burbujaSucia");
				}else{
					changeRoute("burbujaSuciaL");
				}
			}
		}
	}
	
	public void movimiento(int screenX, int screenY){
		
		updateYSpeed(screenY);
		updateXSpeed(screenX);
		
		posY += ySpeed;
		posX += xSpeed;
		
	}
	
	public Burbuja createBurbuja(Projectil bala){
		Random r = new Random();
		int n = Math.abs(r.nextInt()%10);
		if(n <= 2) {
			return new Burbuja(bala.getPosX(), bala.getPosY(), 25, .1, "burbuja4");
		}else if(n <= 7) {
			return new Burbuja(bala.getPosX(), bala.getPosY(), 50, .1, "burbuja3");
		}else{
			return new Burbuja(bala.getPosX(), bala.getPosY(), 100, .1, "burbuja2");
		}
	}
	
	public double getPosX(){
		return posX;
	}
	
	public double getPosY(){
		return posY;
	}
	
	public int getSize(){
		return size;
	}
	
	public boolean isDefeated(){
		if(hits >= 10){
			return true;
		}
		return false;
	}
	
	public void hit(){
		hits++;
	}
	
	public boolean colideMan(Personaje personaje){
		Rectangle p = new Rectangle(personaje.getPosX(), personaje.getPosY(), personaje.getSizeX(), personaje.getSizeY());
		Rectangle bs = new Rectangle(this.posX, this.posY, this.size, this.size);
		if(bs.intersects(p)){
			SoundLoader.getInstance().play("sounds/hit.wav");
			return true;
		}
		return false;
	}
	
	public boolean hitByBullet(Projectil bullet){
		Rectangle bala = new Rectangle(bullet.getPosX(), bullet.getPosY(), bullet.getSizeX(), bullet.getSizeY());
		Rectangle bs = new Rectangle(this.posX, this.posY, this.size, this.size);
		if(bala.intersects(bs)){	
			hits++;
			return true;
		}
		return false;
	}
	
	public double getXSpeed(){
		return xSpeed;
	}
	
	public void changeRoute(String route) {
		burbuja.changeRoute(route);
		crash = true;
	}

}
