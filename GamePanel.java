import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class GamePanel extends JPanel implements Runnable, MouseListener{

    private static final long serialVersionUID = 1L;
    private int PWIDTH = 800;
    private int PHEIGHT =  600;

    private Thread animator;
    private volatile boolean running = false;
    private volatile boolean gameOver = false;
    private volatile boolean isPaused = false;
    private GameContext context;
    private JButton button1;
    private JTextField text;


    public GamePanel() {
        addMouseListener(this);
        setBackground(Color.black);
        this.revalidate();
        setPreferredSize(new Dimension(PWIDTH, PHEIGHT));
        setFocusable(true);
        requestFocus();
        addKeyListener( new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
            	readyForTermination(e);
            	context.keyPressed(e);
            }
        });
        addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                testPress(e.getX(), e.getY());
                context.click(e);
            }
        });
        context = new GameContext();
    }//GamePanel()

    public void addNotify()	{
        super.addNotify();
        startGame();
    }//addNotify

    private void startGame(){
        if(animator == null || !running){
            animator = new Thread(this);
            animator.start();
        }
    }//startGame()

    public void stopGame(){
        running = false;
    }//stopGame()

    public void run(){
        running = true;
        while(running) {
            gameUpdate(PWIDTH, PHEIGHT);
            gameRender();
            paintScreen();
            try {
                Thread.sleep(1000/60);
            } catch (InterruptedException ex) {
            }
        }

    }//run()

    private void gameUpdate(int width, int height){

        context.update(width, height);
//        if(context.getCurrent() == context.getGameOver()){
//            gameOver=true;
//            running=false;
//        }
        if(!isPaused && !gameOver){


        }

    }//gameUpdate()

    private Graphics dbg;
    private java.awt.Image dbImage = null;

    private void gameRender(){
        if(dbImage == null)
        {
            dbImage = createImage(PWIDTH,PHEIGHT);
            if(dbImage == null){
                System.out.println("dbImage is null");
                return;
            }else{
                dbg = dbImage.getGraphics();
            }

        }
        dbg.setColor(Color.white);
        dbg.fillRect(0,0,PWIDTH,PHEIGHT);
        context.paint(dbg);
    }


    public void paintComponent(Graphics g){
        super.paintComponent(g);
        if(dbImage != null)
            g.drawImage(dbImage, 0, 0, null);
    }

    private void readyForTermination(KeyEvent e) {
    	try {        	
            if(e.getKeyCode() == KeyEvent.VK_F5) {
                System.exit(0);
            }
        } catch(NullPointerException x){
            System.out.println("Game has ended");
        }
    }// end of readyForTermination()



    private void testPress(int x, int y) {
        if (!gameOver && !isPaused) {


            // Do Something
        }

    }//testPress

    private void paintScreen(){
        Graphics g;
        try{
            g = this.getGraphics();
            if((g != null) && (dbImage != null))
                g.drawImage(dbImage,0,0,null);
            Toolkit.getDefaultToolkit().sync();
        }catch(Exception e){
            System.out.println("Graphics context error: "+e);
        }
    }

    public void pauseGame(){
        isPaused = true;
    }

    public void resumeGame(){
        isPaused = false;
    }

    public JTextField getLetter(){
        return text;
    }

    public JButton getButton(){
        return button1;
    }



    public static void main(String[] args) {
    	ImageLoader.getInstance();
        JFrame app = new JFrame("Problemas Burbujientos");
        GamePanel gp = new GamePanel();
        app.getContentPane().add(gp, BorderLayout.CENTER);
        app.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        app.setUndecorated(true);
	    app.pack();
	    app.setLocationRelativeTo(null);
        app.setResizable(false);
        app.setVisible(true);
        
    }
    
    /*MOUSE LISTENERS*/

	@Override
	public void mouseClicked(MouseEvent me) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent me) {
		// TODO Auto-generated method stub
		
	}
}