import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.Random;

public class Start implements GameState{
	private ConjuntoBurbujas burbujas;
	private Personaje rambo;
    private int count = 0;
    private int wait = 0;
    private int substring = 1;
    public Color AzulMarino = new Color(1, 14, 97);
    public Color VerdeFuerte = new Color(24, 69, 6);
    public GameContext gc;
private Ribbon ribbon;
    
    public Start() {
    	rambo = new Personaje(-60, 400);
    	burbujas = new ConjuntoBurbujas();
    	ribbon = new Ribbon(800, 600, 1);
    	agregaBurbujas();
    	ribbon.stopMoving();
    }
    
    public void agregaBurbujas(){

    	burbujas.agregaBurbuja(new Burbuja(-200, 100, 150, .1, "burbuja1"));
    }
    
    @Override
    public void paint(Graphics g) {
    	count++;
//    	image.paintImage(g,0,0,800,600);
    	ribbon.paint(g);
    	boolean display = false;

    	String text = "PROBLEMAS BURBUJIENTOS";
    	g.setColor(AzulMarino);
    	
    	if(count%6 == 0 && substring < text.length()){
    		substring++;
    	}
    	
    	if(substring == text.length()){
    		wait++;
    		display = true;
    	}
    	
    	Font myFont = new Font("Trebuchet", Font.BOLD, 50);
        g.setFont(myFont);
        g.drawString(text.substring(0, substring), 35, 80);
        
        if(display && wait > 20){
        	rambo.paint(g);
//            player.paintImage(g,340,400,120,150);
            burbujas.paint(g);
//            
            Font myFont3 = new Font("Trebuchet", Font.BOLD, 40);
            g.setFont(myFont3);
            g.drawString("ENTER - Iniciar", 250, 230);
            g.drawString("I  -  Instrucciones", 230, 280);
            g.drawString("F5 - Salir", 310, 330);
        }
    	
        
    }

	@Override
	public void update(int width, int height) {
		if(wait > 20){
			ribbon.moveLeft();
			if(rambo.getPosX() <= 350){
				rambo.muevete(5, 800);
				burbujas.moveRight();
			}else{
				burbujas.bounce();
			}
		}
		ribbon.update();
	}

	@Override
	public void setContext(GameContext gc) {
		this.gc = gc;
	}

	@Override
	public void start() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void instrucciones() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pausa() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void gameover() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void nivel1() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void nivel2() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void burbujaSucia() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void scores() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void tienda() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int keyCode = e.getKeyCode();
		if(keyCode == KeyEvent.VK_I) {
			gc.instrucciones();
		}else if(keyCode == KeyEvent.VK_ENTER) {
			gc.nivel1();
		}
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		Random r = new Random();
		int n = Math.abs(r.nextInt()%4);
		if(n == 0) {
			burbujas.agregaBurbuja(new Burbuja(e.getX()-75, e.getY()-75, 150, .1, "burbuja1"));
		}else if(n == 1) {
			burbujas.agregaBurbuja(new Burbuja(e.getX()-50, e.getY()-50, 100, .1, "burbuja2"));
		}else if(n == 2) {
			burbujas.agregaBurbuja(new Burbuja(e.getX()-25, e.getY()-25, 50, .1, "burbuja3"));
		}else if(n == 3) {
			burbujas.agregaBurbuja(new Burbuja(e.getX()-12, e.getY()-12, 25, .1, "burbuja4"));
		}
		
	}

	@Override
	public void scoreboard() {
		// TODO Auto-generated method stub
		
	}

}
