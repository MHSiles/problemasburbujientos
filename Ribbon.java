import java.awt.Graphics;
import java.awt.image.BufferedImage;

public class Ribbon {
	private BufferedImage im;
	private int width, height;
	private int pWidth, pHeight;
	
	private int moveSize;
	private boolean isMovingRight;
	private boolean isMovingLeft;
	
	private int xImHead;
	
	public Ribbon(int w, int h, int moveSz){
		pWidth = w;
		pHeight = h;
		im = ImageLoader.getInstance().getImage("ribbonImg2");
		width = im.getWidth();
		height = im.getHeight();
		if(width < pWidth)
			System.out.println("La imagen es mas chica que el panel");
		moveSize = moveSz;
		isMovingRight = false;
		isMovingLeft = false;
		xImHead = 0;
	}
	
	public void moveRight(){
		isMovingRight = true;
		isMovingLeft = false;
	}
	
	public void moveLeft(){
		isMovingRight = false;
		isMovingLeft = true;
	}
	
	public void stopMoving(){
		isMovingRight = false;
		isMovingLeft = false;
	}
	
	public void update(){
		if(isMovingRight){
			xImHead = (xImHead + moveSize) % width;
		}else if(isMovingLeft){
			xImHead = (xImHead - moveSize) % width;
		}
	}
	
	public void paint(Graphics g){
		if(xImHead == 0)
			draw(g, im, 0, pWidth, 0, pWidth);
		else if((xImHead > 0) && (xImHead < pWidth)){
			draw(g, im, 0, xImHead, width - xImHead, width); //Dibujas la "cola" de la imagen en el lado izquierdo
			draw(g, im, xImHead, pWidth, 0, pWidth - xImHead); //Dibujas la "cabeza" de la imagen en el lado derecho
		}else if(xImHead >= pWidth){
			draw(g, im, 0, pWidth, width - xImHead, width - xImHead + pWidth);
		}else if((xImHead < 0) && (xImHead >= pWidth - width)){
			draw(g, im, 0, pWidth, -xImHead, pWidth - xImHead);
		}else if(xImHead < pWidth - width){
			draw(g, im, 0, width + xImHead, -xImHead, width);
			draw(g, im, width + xImHead, pWidth, 0, pWidth - width - xImHead);
		}
		
	}
	
	private void draw(Graphics g, BufferedImage im, int scrX1, int scrX2, int imX1, int imX2){
		g.drawImage(im, scrX1, 0, scrX2, height, 
						 imX1, 0,  imX2, pHeight, null);
	}

}
