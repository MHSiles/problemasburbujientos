public class Factory {
    private static Factory instance;

    private Factory() {
    }

    public static Factory getInstance() {
        if (instance == null) {
            instance = new Factory();
        }
        return instance;
    }

    public GameState createState(String state) {
    	
    	if (state.equalsIgnoreCase("start")){
    		return new Start();
    	}else if (state.equalsIgnoreCase("instrucciones")){
    		return new Instrucciones();
    	}else if (state.equalsIgnoreCase("pausa")){
    		return new Pausa();
    	}else if (state.equalsIgnoreCase("gameover")){
    		return new GameOver();
    	}else if (state.equalsIgnoreCase("nivel1")){
    		return new Nivel1();
    	}else if (state.equalsIgnoreCase("nivel2")){
    		return new Nivel2();
    	}else if (state.equalsIgnoreCase("nivel3")){
    		return new Nivel3();
    	}else if (state.equalsIgnoreCase("burbujaSucia")){
    		return new BurbujaSucia();
    	}else if (state.equalsIgnoreCase("tienda")){
    		return new Tienda();
    	}else if (state.equalsIgnoreCase("loading")){
    		return new Loading();
    	}
    	

        return null;

    }
}
