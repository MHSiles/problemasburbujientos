import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.imageio.ImageIO;

public class ImageLoader {
	private final static String DIR = "img/";
	private HashMap<String, ArrayList<BufferedImage>> images;
	private GraphicsConfiguration config;
	
	private static  ImageLoader instance;
	
	private ImageLoader(){
		images = new HashMap<String, ArrayList<BufferedImage>>();
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		config = ge.getDefaultScreenDevice().getDefaultConfiguration();
		loadSingleImage("aon.jpg");
    	loadSingleImage("armour.png");
    	loadSingleImage("armourLeft.png");
    	loadSingleImage("background.png");
    	loadSingleImage("background2.png");
    	loadSingleImage("boss.png");
    	loadSingleImage("burbuja1.png");
    	loadSingleImage("burbuja2.png");
    	loadSingleImage("burbuja3.png");
    	loadSingleImage("burbuja4.png");
    	loadSingleImage("burbujaSucia.png");
    	loadSingleImage("burbujaSucia2.png");
    	loadSingleImage("burbujaSucia2L.png");
    	loadSingleImage("burbujaSuciaL.png");
    	loadSingleImage("countdown.jpg");
    	loadSingleImage("gameover.jpg");
    	loadSingleImage("greyBackground.png");
    	loadSingleImage("heart.png");
    	loadSingleImage("main.jpg");
    	loadSingleImage("player.png");
    	loadSingleImage("playerLeft.png");
    	loadSingleImage("proyectil.png");
    	loadSingleImage("rambo.png");
    	loadSingleImage("ribbonImg2.png");
    	loadSingleImage("start.png");
    	loadSingleImage("vol.png");
    	loadSingleImage("mute.png");
	}
	
	public static ImageLoader getInstance(){
		if(instance == null){
			instance = new ImageLoader();
		}
		return instance;
	}
	
	public boolean loadSingleImage(String fnm){
		String name = getPrefix(fnm);
		if(images.containsKey(name)){
			System.out.println("Error: " + name + " already used");
			return false;
		}
		
		BufferedImage bi = loadImage(fnm);
		if(bi != null){
			ArrayList<BufferedImage> imsList = new ArrayList<BufferedImage>();
			imsList.add(bi);
			images.put(name, imsList);
			System.out.println("Stored " + DIR + fnm);
			return true;
		}else
			return false;
		
	}
	
	private String getPrefix(String fnm){
		int posn;
		if((posn = fnm.lastIndexOf(".")) == -1){
			System.err.println("No prefix found for " + fnm);
			return fnm;
		}else{
			return fnm.substring(0, posn);
		}
	}
	
	private BufferedImage loadImage(String fileName){
		try{
//			BufferedImage image = ImageIO.read(new File(DIR+fileName));
			BufferedImage image = ImageIO.read(getClass().getResourceAsStream(DIR + fileName));
			int transparency = image.getColorModel().getTransparency();
			BufferedImage copy = config.createCompatibleImage(image.getWidth(), image.getHeight(), transparency);
			Graphics2D g2d = copy.createGraphics();
			g2d.drawImage(image, 0, 0, null);
			g2d.dispose();
			return copy;
		}catch(IOException e){
			return null;
		}
		
	}
	
	public BufferedImage getImage(String name){
		ArrayList<BufferedImage> imsList = (ArrayList<BufferedImage>) images.get(name);
		if(imsList == null){
			System.out.println("No images stored under " + name);
			return null;
		}
		
		return (BufferedImage) imsList.get(0);
	}

}
