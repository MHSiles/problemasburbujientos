import java.awt.Graphics;

public class Burbuja{
	private int size, posX, posY;
	private double ySpeed, xSpeed, gravity;
	private double maxJumpSpeed;
	private boolean firstBounce;
	private Image burbuja;
	
	public Burbuja(int x, int y, int size, double gravity, String imagen){
		burbuja = new Image(imagen);
		posX = x;
		posY = y;
		this.gravity = gravity;
		firstBounce = true;
		this.size = size;
		ySpeed = 0;
		xSpeed = 1;
	}
	
	public Burbuja(int x, int y, int size, double gravity, String imagen, int dir){
		burbuja = new Image(imagen);
		posX = x;
		posY = y;
		this.gravity = gravity;
		firstBounce = true;
		this.size = size;
		ySpeed = 0;
		xSpeed = dir;
	}
	
	public void paint(Graphics g){
		
		burbuja.paintImage(g, posX, posY, size, size);
	}
	
	private void updateYSpeed(int screenY) {
		if(posY + size >= screenY-50) {
			if(firstBounce) {
				//Guardar la velocidad inicial para cada rebote
				maxJumpSpeed = -ySpeed;
				firstBounce = false;
			}
			
			//Resetear la velocidad para subir
			ySpeed = maxJumpSpeed;
		}
		ySpeed += gravity;
	}
	
	private void updateXSpeed(int screenX) {
		//Si llega a alguno de los extremos laterales
		if((posX + size >= screenX) || (posX <= 0)) {
			xSpeed = xSpeed * -1;
		}	
	}
	
	public void movimiento(int screenX, int screenY){
		
		updateYSpeed(screenY);
		updateXSpeed(screenX);
		
		posY += ySpeed;
		posX += xSpeed;
		
	}
	
	public void moveRight(){
		updateYSpeed(600);
		posY += ySpeed;
		posX += 4;
	}
	
	public void bounce(){
		updateYSpeed(600);
		posY += ySpeed;
	}
	
	public double getPosX(){
		return posX;
	}
	
	public double getPosY(){
		return posY;
	}
	
	public int getSizeX(){
		return size;
	}
	
	public int getSizeY(){
		return size;
	}
}