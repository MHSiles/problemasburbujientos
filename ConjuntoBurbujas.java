import java.awt.*;
import java.util.*;
import java.util.List;


public class ConjuntoBurbujas {
	
    private List<Burbuja> burbujas;
    private List<Burbuja> newBurbujas;
    private List<Burbuja> removeBurbujas;
    

    public ConjuntoBurbujas(){
        burbujas = Collections.synchronizedList(new LinkedList<Burbuja>());
        newBurbujas = Collections.synchronizedList(new LinkedList<Burbuja>());
        removeBurbujas = Collections.synchronizedList(new LinkedList<Burbuja>());
    }

    public void agregaBurbuja(Burbuja b){
        burbujas.add(b);
    }
    
    public void movimiento(int width, int height){
    	for(int i = 0; i < burbujas.size(); i++){
    		Burbuja burbuja = burbujas.get(i);
    		burbuja.movimiento(width, height);
    	}
    }
    
    public void moveRight(){
    	for(int i = 0; i < burbujas.size(); i++){
    		Burbuja burbuja = burbujas.get(i);
    		burbuja.moveRight();
    	}
    }
    
    public void bounce(){
    	for(int i = 0; i < burbujas.size(); i++){
    		Burbuja burbuja = burbujas.get(i);
    		burbuja.bounce();
    	}
    }
    
    public int getSize(){
    	return burbujas.size();
    }
    
    public boolean colideMan(Personaje personaje){
    	
    	Rectangle rectP = new Rectangle(personaje.getPosX(), personaje.getPosY(), personaje.getSizeX(), personaje.getSizeY());
    	
    	Iterator<Burbuja> i = burbujas.iterator();

        while(i.hasNext()){
            Burbuja burbuja = i.next();

            Rectangle rectB = new Rectangle((int)burbuja.getPosX(), (int)burbuja.getPosY(), burbuja.getSizeX(), burbuja.getSizeY());
            if(Scores.getInstance().hasArmour() && burbuja.getSizeX() == 25){
            	
            }else if(rectB.intersects(rectP)){
            	SoundLoader.getInstance().play("sounds/hit.wav");
            	return true;
            }
        }
        
        return false;
    	
    }
    
    public boolean hitByBullet(Projectil bullet) {
    	boolean hit = false;
    	Rectangle bala = new Rectangle(bullet.getPosX(), bullet.getPosY(),  bullet.getSizeX(), bullet.getSizeY());
    	
    	Iterator<Burbuja> i;
    	for(int j = 0; j < burbujas.size(); j++){
    		Burbuja burbuja = burbujas.get(j);
    		Rectangle burb = new Rectangle((int)burbuja.getPosX(), (int)burbuja.getPosY(), burbuja.getSizeX(), burbuja.getSizeY());
    		if(burb.intersects(bala)) {
    			hitBurbuja(burbuja);
    			hit = true;
    			SoundLoader.getInstance().play("sounds/pop.wav");
    		}
    	}
    	if(hit) {
    		i = newBurbujas.iterator();
    		while(i.hasNext()) {
    			Burbuja newB = i.next();
    			burbujas.add(newB);
    		}
    		newBurbujas.clear();
    		i = removeBurbujas.iterator();
    		while(i.hasNext()) {
    			Burbuja newB = i.next();
    			burbujas.remove(newB);
    		}
    		removeBurbujas.clear();
    	}
    	return hit;
    }

    public void hitBurbuja(Burbuja b) {
    	if(b.getSizeX() == 25) {
    		burbujas.remove(b);
    	}else if(b.getSizeX() == 50) {
    		newBurbujas.add(new Burbuja((int)b.getPosX(), (int)b.getPosY(), 25, .1, "burbuja4", 1));
    		newBurbujas.add(new Burbuja((int)b.getPosX(), (int)b.getPosY(), 25, .1, "burbuja4", -1));
    		removeBurbujas.add(b);
    	}else if(b.getSizeX() == 100) {
    		newBurbujas.add(new Burbuja((int)b.getPosX(), (int)b.getPosY(), 50, .1, "burbuja3", 1));
    		newBurbujas.add(new Burbuja((int)b.getPosX(), (int)b.getPosY(), 50, .1, "burbuja3", -1));
    		removeBurbujas.add(b);
    	}
    	else if(b.getSizeX() == 150) {
    		newBurbujas.add(new Burbuja((int)b.getPosX(), (int)b.getPosY(), 100, .1, "burbuja2", 1));
    		newBurbujas.add(new Burbuja((int)b.getPosX(), (int)b.getPosY(), 100, .1, "burbuja2", -1));
    		removeBurbujas.add(b);
    	}
    }
    
    public void paint(Graphics g){
    	for(int i = 0; i < burbujas.size(); i++) {
    		burbujas.get(i).paint(g);
    	}
    }

}