import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

public class GameOver implements GameState{
    private GameContext gc;
    private Image background, rambo;

    public GameOver(){
    	background = new Image("gameover");
    	rambo = new Image("rambo");
    }

	@Override
	public void paint(Graphics g) {
		Font myFont = new Font("Trebuchet", Font.BOLD, 50);
		Font myFont2 = new Font("Trebuchet", Font.BOLD, 70);
		if(Scores.getInstance().hasWon()) {
			g.setFont(myFont);
	        g.setColor(Color.BLACK);
	        g.fillRect(0,0,800,600);
	        g.setColor(Color.WHITE);
	        g.drawString("Venciste a la", 230, 100);
			g.setFont(myFont2);
			g.drawString("BURBUJA SUCIA", 100, 200);
			rambo.paintImage(g, 300, 230, 200, 200);
			g.setFont(myFont);
			g.drawString("Puntuaci�n Final: " + Scores.getInstance().getScore(), 140, 500);
		}else {
	        g.setFont(myFont2);
			background.paintImage(g, 0, 0, 800, 600);
			g.drawString("Comes tierra", 170, 100);
			g.setFont(myFont);
			g.drawString("Puntuación Final: " + Scores.getInstance().getScore(), 140, 450);
			g.drawString("Presiona [ F5 ] para salir", 100, 500);
			g.setFont(new Font("Trebuchet", Font.BOLD, 30));
			g.drawString("Presiona Enter para volver a jugar", 150, 550);
		}
		
	}

	@Override
	public void update(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setContext(GameContext gc) {
		this.gc = gc;
	}

	@Override
	public void start() {
		gc.start();
		
	}

	@Override
	public void instrucciones() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pausa() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void gameover() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void nivel1() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void nivel2() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void burbujaSucia() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void scores() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void tienda() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_ENTER){
//			Scores.getInstance().reset();
//			System.out.println("restart");
//			gc.restart();		
			gc.start();
		}
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void scoreboard() {
		// TODO Auto-generated method stub
		
	}
    
    


}
