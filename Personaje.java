import java.awt.Graphics;

public class Personaje{
	
	private int posX, posY, sizeX, sizeY;
	private Image personaje, armour;
	
	public Personaje(int x, int y){
		personaje = new Image("player");
		armour = new Image("armour");
		posX = x;
		posY = y;
		sizeX = 50;
		sizeY = 75;
	}
	
	public void paint(Graphics g){
		if(Scores.getInstance().hasArmour()) {
			armour.paintImage(g, posX, posY, sizeX, sizeY);
		}else {
			personaje.paintImage(g, posX, posY, sizeX, sizeY);
		}
	}
	
	public void muevete(int x, int screen){
		
		posX += x;
		
		if(x < 0 && personaje.getRoute().equalsIgnoreCase("player")) {
			personaje.changeRoute("playerLeft");
			armour.changeRoute("armourLeft");
		}else if(x > 0 && personaje.getRoute().equalsIgnoreCase("playerLeft")) {
			personaje.changeRoute("player");
			armour.changeRoute("armour");
		}else if(x < 0 && personaje.getRoute().equalsIgnoreCase("armour")) {
			personaje.changeRoute("armourLeft");
		}else if(x > 0 && personaje.getRoute().equalsIgnoreCase("armourLeft")) {
			personaje.changeRoute("armour");
		}
		
		if(posX > screen){
			posX = screen;
		}
		
		if(posX < 0){
			posX = 0;
		}
	}
	
	public int getPosX(){
		return posX;
	}
	
	public int getPosY(){
		return posY;
	}
	
	public int getSizeX(){
		return sizeX;
	}
	
	public int getSizeY(){
		return sizeY;
	}
	
	public String getRoute() {
		return personaje.getRoute();
	}
	
	public void changeRoute(String route) {
		personaje.changeRoute(route);
	}
}