
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

public class GameContext {
	private Loading loading;
	private GameState start;
    private GameState instrucciones;
    private GameState pausa;
    private GameState gameover;
    
    private GameState nivel1;
    private GameState nivel2;
    private GameState nivel3;
    private GameState burbujaSucia;
    
//    private GameState scores;
    private GameState tienda;
    
    private GameState current;
    
    private int level, i;

    public GameContext(){
    	loading = (Loading)Factory.getInstance().createState("loading");
    	i = 10000000;
    	while(i > 0) {
    		i--;
    	}
    	start = Factory.getInstance().createState("start");
    	instrucciones = Factory.getInstance().createState("instrucciones");
    	pausa = Factory.getInstance().createState("pausa");
    	gameover = Factory.getInstance().createState("gameover");
    	
    	nivel1 = Factory.getInstance().createState("nivel1");
    	nivel2 = Factory.getInstance().createState("nivel2");
    	nivel3 = Factory.getInstance().createState("nivel3");
    	burbujaSucia = Factory.getInstance().createState("burbujaSucia");
    	
//    	scores = Factory.getInstance().createState("scores");
    	tienda = Factory.getInstance().createState("tienda");

    	/*******************************/
    	
    	loading.setContext(this);
    	start.setContext(this);
    	instrucciones.setContext(this);
    	pausa.setContext(this);
    	gameover.setContext(this);
    	
    	nivel1.setContext(this);
    	nivel2.setContext(this);
    	nivel3.setContext(this);
    	burbujaSucia.setContext(this);
    	
//    	scores.setContext(this);
    	tienda.setContext(this);
    	

        current = loading;
        level = 1;
    }
    
    /*GETTERS*/
    
    public GameState getStart(){
        return start;
    }
    
    public GameState getInstrucciones(){
        return instrucciones;
    }
    
    public GameState getPausa(){
        return pausa;
    }
    
    public GameState getGameOver(){
        return gameover;
    }
    
    public GameState getNivel1(){
        return nivel1;
    }
    
    public GameState getNivel2(){
        return nivel2;
    }
    
    public GameState getBurbujaSucia(){
        return burbujaSucia;
    }
    
//    public GameState getScores(){
//        return scores;
//    }
    
    public GameState getTienda(){
        return tienda;
    }
    
    public GameState getCurrent() {
        return current;
    }
    
    /*SETTERS*/
    
    public void setCurrent(GameState gamestate){
        current = gamestate;
    }
    
    /*FUNCIONES*/

    public void update(int width, int height){
        current.update(width, height);
    }

    public void paint(Graphics g){
        current.paint(g);
    }

    public void start(){
    	current = start;
    }
    
    public void instrucciones(){
    	current = instrucciones;
    }
    
    public void pausa(){
    	if(current != pausa) {
    		current = pausa;
    	}else if(level == 1){
    		current = nivel1;
    	}else if(level == 2) {
    		current = nivel2;
    	}else if(level == 3) {
    		current = nivel3;
    	}else if(level == 4) {
    		current = burbujaSucia;
    	}
    }
    
    public void gameover(){
    	current = gameover;    	
    }
	
    public void nivel1(){
    	current = nivel1;    	
    }
    
    public void nivel2(){
    	current = nivel2;    	
    }
    
    public void nivel3(){
    	current = nivel3;    	
    }
    
    public void burbujaSucia(){
    	current = burbujaSucia;    	
    }
	
//    public void scores(){
//    	current = scores;    	
//    }
    
    public void tienda(){
    	current = tienda;    	
    }
    
    public void keyPressed(KeyEvent e) {
    	if(e.getKeyCode() == KeyEvent.VK_M){
    		Scores.getInstance().mute();
    	}
    	current.keyPressed(e);
    }

    public void click(MouseEvent e) {
    	current.mousePressed(e);
    }
    
    public void restartLevel(){
    	if(current == nivel1){
    		nivel1 = Factory.getInstance().createState("nivel1");
    		nivel1.setContext(this);
    		current = nivel1;
    	}else if(current == nivel2){
    		nivel2 = Factory.getInstance().createState("nivel2");
    		nivel2.setContext(this);
    		current = nivel2;
    	}else if(level == 3){
    		nivel3 = Factory.getInstance().createState("nivel3");
    		nivel3.setContext(this);
    		current = nivel3;
    	}else if(level == 4){
    		burbujaSucia = Factory.getInstance().createState("burbujaSucia");
    		burbujaSucia.setContext(this);
    		current = burbujaSucia;
    	}
    }
    
    public void nextLevel(){
    	if(level == 1){
    		level++;
    		nivel2();
    	}else if(level == 2){
    		level++;
    		nivel3();
    	}else if(level == 3) {
    		level++;
    		burbujaSucia();
    	}
    }
    
    public void restart(){
    	start = Factory.getInstance().createState("start");
    	instrucciones = Factory.getInstance().createState("instrucciones");
    	pausa = Factory.getInstance().createState("pausa");
    	
    	nivel1 = Factory.getInstance().createState("nivel1");
    	nivel2 = Factory.getInstance().createState("nivel2");
    	nivel3 = Factory.getInstance().createState("nivel3");
    	burbujaSucia = Factory.getInstance().createState("burbujaSucia");
    	
//    	scores = Factory.getInstance().createState("scores");
    	tienda = Factory.getInstance().createState("tienda");

    	/*******************************/
    	
    	start.setContext(this);
    	instrucciones.setContext(this);
    	pausa.setContext(this);
    	
    	nivel1.setContext(this);
    	nivel2.setContext(this);
    	nivel3.setContext(this);
    	burbujaSucia.setContext(this);
    	
//    	scores.setContext(this);
    	tienda.setContext(this);
    	
        level = 1;
    }


}
