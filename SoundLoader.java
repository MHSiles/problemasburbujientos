import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class SoundLoader implements Runnable{
	private static SoundLoader instance;
	private Clip clip;
	private String path;
	
	private SoundLoader(){
		try{
			clip = AudioSystem.getClip();
		}catch(Exception e){
		}
	}
	
	public void run(){
		playSound(path);
	}
	
	public void play(String path){
		if(Scores.getInstance().hasSound()){
			Thread thread = new Thread(this);
			this.path = path;
			thread.start();
		}	
	}
	
	public static SoundLoader getInstance(){
		if(instance == null){
			instance = new SoundLoader();
		}
		return instance;
	}
	
	public void playSound(String path){
		
		try{
			clip.close();
			clip.open(AudioSystem.getAudioInputStream(GamePanel.class.getResource(path)));
			clip.start();
				
				
		}catch(Exception e){
			clip.close();
			System.err.println(e.getMessage());
			System.err.println("No corrio el sonido");
		}
		
	}
	
}