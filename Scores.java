import java.awt.Font;
import java.awt.Graphics;

public class Scores{
	private int currentScore;
	private int lives, x;
	private boolean armour, won;
	private static Scores instance;
	private Image life, soundImg, mute;
	private boolean sound;
	
	private Scores() {
		sound = true;
		lives = 3;
		currentScore = 0;
		armour = false;
		life = new Image("heart");
		soundImg = new Image("vol");
		mute = new Image("mute");
		x = 575;
		won = false;
	}
	
	public static Scores getInstance() {
		if(instance == null) {
			instance = new Scores();
		}
		return instance;
	}
	
	public void paintNivel(Graphics g) {
        g.setFont(new Font("Trebuchet", Font.BOLD, 25));
        g.drawString("Puntuacion: " + currentScore, 575, 30);
        for(int i = 0; i < lives; i++) {
        	life.paintImage(g, x, 60, 25, 25);
        	x+=25;
        }
        if(sound){
        	soundImg.paintImage(g, 10, 10, 20, 20);
        }else{
        	mute.paintImage(g, 10, 10, 20, 20);
        }
        x = 575;
        
//        g.drawString("Lives: " + lives, 575, 60);
	}
	
	
	
	public void addPoints(int score){
		currentScore += Math.abs(score);
	}
	
	public int getScore(){
		return currentScore;
	}
	

	
	public boolean pay(int price) {
		if(price <= currentScore) {
			currentScore -= price;
			return true;
		}
		return false;
	}
	
	public void reset() {
		instance = null;
	}

	public int loseLife(){
		lives -= 1;
		return lives;
	}
	
	public void addLife(){
		lives += 1;
	}
	
	public int getLives() {
		return lives;
	}
	
	public void buyArmour(){
		if(!armour){
			armour = true;
		}
	}
	
	public boolean hasArmour(){
		return armour;
	}
	
	public boolean hasWon() {
		return won;
	}
	
	public void win(){
		won = true;
	}
	
	public boolean hasSound(){
		return sound;
	}
	
	public void mute(){
		sound = !sound;
		System.out.println("Sound: " + sound);
	} 
}
