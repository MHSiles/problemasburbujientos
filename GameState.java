import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

public interface GameState {
	
    void paint(Graphics g);
    void update(int width, int height);
    void setContext(GameContext gc);
	
	void start();
	void instrucciones();
	void pausa();
	void gameover();
	
	void nivel1();
	void nivel2();
	void burbujaSucia();
	void scoreboard();
	
	void scores();
	void tienda();
	
	void keyPressed(KeyEvent e);
	void mousePressed(MouseEvent e);
	
}
