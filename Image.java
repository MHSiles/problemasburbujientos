import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

public class Image extends JPanel{
	
	private static final long serialVersionUID = 1L;
    public BufferedImage image;
	private String route;
    
    
	public Image(String route){
		this.image = ImageLoader.getInstance().getImage(route);
		this.route = route;
	}
	
	public void paintImage(Graphics gra, int initX, int initY, int x, int y){
		gra.drawImage(image, initX, initY, x, y, this); 
	} 
	
	public void changeRoute(String route){
			this.image = ImageLoader.getInstance().getImage(route);
			this.route = route;

	}
	
	public String getRoute() {
		return this.route;
	}
}