import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

public class Loading implements GameState{
	private GameContext gc;
	private int count, substring, loops;
	
	public Loading() {
		count = 0;
		substring = 0;
		loops = 0;
	}
	
	public void load() {

	}
	
	@Override
	public void paint(Graphics g) {
		count++;
		
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, 800, 600);
		g.setColor(Color.WHITE);
		g.setFont(new Font("Trebuchet", Font.BOLD, 50));
		g.drawString("Cargando", 240, 295);
//		g.drawString("...", 480, 275);
		
		String dots  = "...";
		
		if(count%30 == 0 && substring < dots.length()) {
			substring++;
		}
		
		g.drawString(dots.substring(0, substring), 480, 295);
		
		if(count%60 == 0 && substring == dots.length()) {
			substring = 0;
			loops++;
		}
		
	}

	@Override
	public void update(int width, int height) {
		if(loops >= 2) {
			gc.start();
		}
	}

	@Override
	public void setContext(GameContext gc) {
		this.gc =  gc;
		
	}

	@Override
	public void start() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void instrucciones() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pausa() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void gameover() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void nivel1() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void nivel2() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void burbujaSucia() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void scoreboard() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void scores() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void tienda() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

}
