import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.Color;
import java.awt.Font;

public class Instrucciones implements GameState{

	GameContext gc;
	Image background;
	
	public Instrucciones(){
		background = new Image("greyBackground");
	}

	@Override
	public void paint(Graphics g) {
		background.paintImage(g, 0, 0, 800, 600);

		g.setColor(Color.black);		
		Font myFont = new Font("Trebuchet", Font.BOLD, 50);
        g.setFont(myFont);
        g.drawString("INSTRUCCIONES", 200, 100);
        Font myFont1 = new Font("Trebuchet", Font.BOLD, 30);
        g.setFont(myFont1);
        g.drawString("La Burbuja Sucia obligo a tu pueblo a comer tierra.", 50, 170);
        g.drawString("Es hora de que alguien reviente a las burbujas.", 70, 200);
        g.drawString("Destruye a todas las burbujas en cada zona para", 55, 240);
        g.drawString("avanzar y acercarte mas a la burbuja sucia.", 80, 270);
        g.drawString("Consigue una armadura para protegerte de", 80, 310);
        g.drawString("algunas burbujas.", 250, 340);
        Font myFont2 = new Font("Trebuchet", Font.BOLD, 40);
        g.setFont(myFont2);
        g.drawString("Movimiento [ <- ][ -> ] ", 190, 390);
        g.drawString("Disparo [ space ]", 230, 430);
        g.drawString("Sonido [ M ]", 290, 470);
        g.drawString("Pausa [ P ]", 295, 510);
        g.drawString("Regresar [ I ]", 280, 550);
		
	}

	@Override
	public void update(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setContext(GameContext gc) {
		this.gc =  gc;
	}

	@Override
	public void start() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void instrucciones() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pausa() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void gameover() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void nivel1() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void nivel2() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void burbujaSucia() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void scores() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void tienda() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int keyCode = e.getKeyCode();
		if(keyCode == KeyEvent.VK_I) {
			gc.start();
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void scoreboard() {
		// TODO Auto-generated method stub
		
	}
	

}
