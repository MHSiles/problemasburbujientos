import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

public class Tienda implements GameState{

	private Image background, heart, armour;
	private GameContext gc;
	private int count = 0;
	private boolean displayMessage = false, livesMessage = false;
	
	public Tienda(){
		background = new Image("aon");
		heart = new Image("heart");
		armour = new Image("armour");
	}
	
	@Override
	public void paint(Graphics g) {
		background.paintImage(g, 0, 0, 800, 600);
		armour.paintImage(g, 170, 180, 180, 240);
		heart.paintImage(g, 450, 230, 160, 160);
		Scores.getInstance().paintNivel(g);
		g.setColor(Color.white);
        g.setFont(new Font("Trebuchet", Font.BOLD, 50));
        g.drawString("Tienda", 320, 100);
        g.setFont(new Font("Trebuchet", Font.BOLD, 30));
        g.drawString("Presiona una tecla para comprar", 170, 150);
        g.drawString("X - Armadura", 160, 450);
        g.drawString("C - Vida +1", 455, 450);
        g.drawString("Presiona ENTER para avanzar de nivel", 135, 570);
        g.setFont(new Font("Trebuchet", Font.BOLD, 20));
        g.drawString("(300 puntos)", 185, 480);
        g.drawString("(70 puntos)", 475, 480);
        if(displayMessage){
        	count++;
        	if(count < 50){
        		g.setColor(Color.RED);
                g.fillRect(215,280,350,70);
                g.setColor(Color.WHITE);
                g.drawString("YA COMPRASTE LA ARMADURA", 240, 320);
            }else{
            	displayMessage = false;
            	count = 0;
            }
        }else if(livesMessage) {
        	count++;
        	if(count < 50) {
        		g.setColor(Color.RED);
                g.fillRect(215,280,400,70);
                g.setColor(Color.WHITE);
                g.drawString("NO PUEDES COMPRAR MAS VIDAS", 240, 320);
        	}else {
        		livesMessage = false;
        		count = 0;
        	}
        }
        
        
	}

	@Override
	public void update(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setContext(GameContext gc) {
		this.gc = gc;
		
	}

	@Override
	public void start() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void instrucciones() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pausa() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void gameover() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void nivel1() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void nivel2() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void burbujaSucia() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void scores() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void tienda() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int keyCode = e.getKeyCode();
		if(keyCode == KeyEvent.VK_C){
			if(Scores.getInstance().getLives() < 9) {
				if(Scores.getInstance().pay(70)){
					Scores.getInstance().addLife();
				}
			}else {
				livesMessage = true;
				count = 0;
			}
		}else if(keyCode == KeyEvent.VK_X){
			if(!Scores.getInstance().hasArmour()){
				if(Scores.getInstance().pay(300)){
					Scores.getInstance().buyArmour();
				}
			}else{
				displayMessage = true;
				count = 0;
			}
		}else if(keyCode == KeyEvent.VK_ENTER){
			gc.nextLevel();
		}
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void scoreboard() {
		// TODO Auto-generated method stub
		
	}

}
