import java.awt.*;

public class Jugador {

	private Image avatar, ganador, perdedor;
    private boolean win, lose;

    public Jugador(String avatar){
    	this.avatar = new Image(avatar);
        this.win = false;
        this.lose = false;
    }
    
    public void paint(Graphics g){
    	Font myFont = new Font("Trebuchet", Font.BOLD, 40);
        g.setFont(myFont);
        g.setColor(Color.WHITE);
        
    	if(!win && !lose){
    		avatar.paintImage(g, 25, 25, 75, 75);
//    		g.drawString("Vidas: " + vidas, 400, 80);
    	}else if(win){
    		ganador.paintImage(g, 0, 0, 600, 600);
    	}else if(lose){
    		perdedor.paintImage(g, 0, 0, 600, 600);
    	}
    }

//    public int getVidas(){
//        return vidas;
//    }
    
    public boolean getWin(){
    	return win;
    }
    
    public boolean getLose(){
    	return lose;
    }

//    public void perderVida(){
//        vidas--;
//        if(vidas == 0){
//        	lose = true;
//        }
//    }
    
    public void perder(){
    	lose = true;
    }
    
    public void ganar(){
    	win = true;
    }
    
}
