import java.awt.Graphics;

public class Projectil {
	private int posX, posY, sizeX, sizeY;
	private Image img;
	
	public Projectil(int x) {
		img = new Image("proyectil");
		posX = x;
		posY  = 450;
		sizeX = 10;
		sizeY = 20;
	}
	
	public void paint(Graphics g) {
		img.paintImage(g, posX, posY, sizeX, sizeY);
	}
	
	public void muevete() {
		posY -=  8;
	}
	
	public int getPosY() {
		return posY;
	}
	
	public int getPosX() {
		return posX;
	}
	
	public int getSizeY() {
		return sizeY;
	}
	
	public int getSizeX() {
		return sizeX;
	}
	
}
