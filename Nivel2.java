import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

public class Nivel2 implements GameState{
	
	public GameContext gc;
    private int contador, time;
	private ConjuntoBurbujas burbujas;
	private Personaje rambo;
	private Image background;
	private Projectil bullet;
	private boolean hasStarted;

	public Nivel2(){
		hasStarted = false;
		contador = 0;
		time = 0;
		burbujas = new ConjuntoBurbujas();
		rambo = new Personaje(380, 470);
		System.out.println(Scores.getInstance().hasArmour());
		background = new Image("background2");
		agregaBurbujas();
	}
	
	public void agregaBurbujas(){

    	burbujas.agregaBurbuja(new Burbuja(1, 200, 100, .15, "burbuja2"));
    	burbujas.agregaBurbuja(new Burbuja(700, 200, 100, .15, "burbuja2"));
    	
    }
	
	@Override
	public void paint(Graphics g) {
		background.paintImage(g, 0, 0, 800, 700);

        g.setColor(Color.RED);
        g.fillRect(0,580, 800, 20);
        g.setColor(Color.BLUE);
        g.fillRect(0,580, 800-(time/3), 20);
		g.setColor(Color.black);
		Scores.getInstance().paintNivel(g);
        g.setFont(new Font("Trebuchet", Font.BOLD, 50));
		if(contador < 60) {
			g.drawString("3", 380, 200);
		}else if(contador < 120) {
			g.drawString("2", 380, 200);
		}else if(contador < 180) {
			g.drawString("1", 380, 200);
		}else {
			hasStarted = true;
		}
		rambo.paint(g);
		burbujas.paint(g);
		if(bullet != null)
			bullet.paint(g);
		
	}

	@Override
	public void update(int width, int height) {
		if(hasStarted){
			time++;	
		}
		
		if(contador < 180) {
			contador++;
		}else {
			burbujas.movimiento(width, height);
			if(burbujas.colideMan(rambo)){
				System.out.println("Colisi�n: Rambo");
				if(Scores.getInstance().loseLife() <= 0){
					gc.gameover();
				}else{
					restartLevel();
				}
			}
			if(bullet != null) {
				bullet.muevete();
				if(bullet.getPosY() <= 0) {
					bullet = null;
				}else if(burbujas.hitByBullet(bullet)) {
					bullet = null;
					Scores.getInstance().addPoints(10);
					System.out.println("Colisi�n: Bala");
				}
			}
			if(burbujas.getSize() == 0) {
				Scores.getInstance().addPoints((800-time)/5);
				gc.tienda();
			}
		}
	}

	@Override
	public void setContext(GameContext gc) {
		this.gc = gc;
		
	}

	@Override
	public void start() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void instrucciones() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pausa() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void gameover() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void nivel1() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void nivel2() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void burbujaSucia() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void scores() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void tienda() {
		// TODO Auto-generated method stub
		
	}
	
	public void restartLevel(){
		gc.restartLevel();
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int keyCode = e.getKeyCode();
		if(hasStarted) {
			if(keyCode == KeyEvent.VK_LEFT) {
				rambo.muevete(-5, 750);
			}else if(keyCode == KeyEvent.VK_RIGHT) {
				rambo.muevete(5, 750);
			}else if(keyCode == KeyEvent.VK_SPACE && bullet == null) {
				if(rambo.getRoute().equalsIgnoreCase("player") || rambo.getRoute().equalsIgnoreCase("armour")) {
					bullet = new Projectil(rambo.getPosX() + 40);
				}else {
					bullet = new Projectil(rambo.getPosX());
				}
			}else if(keyCode == KeyEvent.VK_P) {
				gc.pausa();
			}
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void scoreboard() {
		// TODO Auto-generated method stub
		
	}
}
